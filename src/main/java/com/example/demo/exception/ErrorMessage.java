package com.example.demo.exception;


import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
public class ErrorMessage {
    private int code;
    private HttpStatus status;
    private Date timestamp;
    private String message;
    private String description;

    public static ErrorMessage ERROR_GET_EMPLOYEE = new ErrorMessage(HttpStatus.NOT_FOUND, "ERROR_GET_EMPLOYEE");
    public static ErrorMessage ERROR_ADD_EMPLOYEE = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR_ADD_EMPLOYEE");
    public static ErrorMessage ERROR_UPDATE_EMPLOYEE = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR_UPDATE_EMPLOYEE");
    public static ErrorMessage ERROR_DELETE_EMPLOYEE = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR_DELETE_EMPLOYEE");

    public ErrorMessage(HttpStatus status, Date timestamp, String message, String description) {
        this.code = status.value();
        this.status = status;
        this.timestamp = timestamp;
        this.message = message;
        this.description = description;
    }

    public ErrorMessage(HttpStatus status, String message) {
        this.code = status.value();
        this.status = status;
        this.message = message;
    }

}

