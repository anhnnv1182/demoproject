package com.example.demo.controller;

import com.example.demo.entity.Employee;
import com.example.demo.payload.response.EmployeeResponse;
import com.example.demo.service.Impl.EmployeeServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {
    @Autowired
    EmployeeServiceImpl employeeService;

    @GetMapping("/employee")
    public ResponseEntity<Page<EmployeeResponse>> getEmployees(Pageable pageable) {
        pageable = PageRequest.of(pageable.getPageNumber(), 1);
        Page<EmployeeResponse> employeeResponses = employeeService.getEmployees(pageable);
        return ResponseEntity.ok(employeeResponses);
    }
    @PostMapping("/employee")
    public ResponseEntity<?> addEmployee(@Valid @RequestBody Employee employee){
        return employeeService.add(employee);
    }

    @PutMapping("/employee")
    public ResponseEntity<?> updateEmployee(@Valid @RequestBody Employee employee){
        return employeeService.update(employee);
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") int employeeId){
        return employeeService.delete(employeeId);
    }


}
