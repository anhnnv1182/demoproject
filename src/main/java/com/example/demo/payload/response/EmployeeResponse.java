package com.example.demo.payload.response;

import com.example.demo.entity.Employee;

import java.util.Date;
public class EmployeeResponse {
    private int id;
    private String name;
    private Date dob;
    private int gender;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EmployeeResponse(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getName();
        this.dob = employee.getDob();
        this.gender = employee.getGender();
        this.description = employee.getDescription();
    }
}
