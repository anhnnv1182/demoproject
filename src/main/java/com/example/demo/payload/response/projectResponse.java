package com.example.demo.payload.response;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class projectResponse<T> {


    public static projectResponse EMPLOYEE_ADD_SUCCESSFULLY = new projectResponse(HttpStatus.OK, "EMPLOYEE_ADD_SUCCESSFULLY");
    public static projectResponse EMPLOYEE_UPDATE_SUCCESSFULLY = new projectResponse(HttpStatus.OK, "EMPLOYEE_UPDATE_SUCCESSFULLY");
    public static projectResponse EMPLOYEE_DELETE_SUCCESSFULLY = new projectResponse(HttpStatus.OK, "EMPLOYEE_DELETE_SUCCESSFULLY");

    private int code;
    private HttpStatus status;
    private String message;
    private T data;

    public projectResponse(projectResponse projectResponse){
        this.code = projectResponse.getStatus().value();
        this.status = projectResponse.getStatus();
        this.message = projectResponse.getMessage();
    }
    public projectResponse(HttpStatus status, String message) {
        this.code = status.value();
        this.status = status;
        this.message = message;
    }
}
