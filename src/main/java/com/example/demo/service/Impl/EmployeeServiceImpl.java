package com.example.demo.service.Impl;

import com.example.demo.exception.ErrorMessage;
import com.example.demo.entity.Employee;
import com.example.demo.payload.response.EmployeeResponse;
import com.example.demo.payload.response.projectResponse;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;
    @Override
    public Page<EmployeeResponse> getEmployees(Pageable pageable) {
        Page<Employee> employees = employeeRepository.findAll(pageable);
        return employees.map(EmployeeResponse::new);
    }

    @Override
    public ResponseEntity<?> add(Employee employee) {
        try{
            Employee addEmployee = employeeRepository.save(employee);

            if (addEmployee == null){
                throw new Exception();
            }
        } catch (Exception e){
            return new ResponseEntity<>(ErrorMessage.ERROR_ADD_EMPLOYEE, ErrorMessage.ERROR_ADD_EMPLOYEE.getStatus());
        }

        return new ResponseEntity<>(projectResponse.EMPLOYEE_ADD_SUCCESSFULLY, projectResponse.EMPLOYEE_ADD_SUCCESSFULLY.getStatus());
    }

    @Override
    public ResponseEntity<?> update(Employee employee) {
        try{
            employeeRepository.findById(employee.getId()).orElseThrow();
            Employee updateEmployee = employeeRepository.save(employee);

            if (updateEmployee == null){
                throw new Exception();
            }
        } catch (Exception e){
            return new ResponseEntity<>(ErrorMessage.ERROR_UPDATE_EMPLOYEE, ErrorMessage.ERROR_UPDATE_EMPLOYEE.getStatus());
        }

        return new ResponseEntity<>(projectResponse.EMPLOYEE_UPDATE_SUCCESSFULLY, projectResponse.EMPLOYEE_UPDATE_SUCCESSFULLY.getStatus());
    }

    @Override
    public ResponseEntity<?> delete(int id) {
        try{
            Employee employee = employeeRepository.findById(id).get();
            if (employee == null){
                return new ResponseEntity<>(ErrorMessage.ERROR_DELETE_EMPLOYEE, ErrorMessage.ERROR_DELETE_EMPLOYEE.getStatus());
            }
            employeeRepository.delete(employee);
        } catch (Exception e){
            return new ResponseEntity<>(ErrorMessage.ERROR_DELETE_EMPLOYEE, ErrorMessage.ERROR_DELETE_EMPLOYEE.getStatus());
        }

        return new ResponseEntity<>(projectResponse.EMPLOYEE_ADD_SUCCESSFULLY, projectResponse.EMPLOYEE_ADD_SUCCESSFULLY.getStatus());
    }
}
