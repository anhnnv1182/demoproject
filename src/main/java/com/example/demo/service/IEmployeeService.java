package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.payload.response.EmployeeResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface IEmployeeService {
    Page<EmployeeResponse> getEmployees(Pageable pageable);
    ResponseEntity<?> add(Employee employee);

    ResponseEntity<?> update(Employee employee);

    ResponseEntity<?> delete(int id);

}
